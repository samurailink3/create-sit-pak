# Create-SIT-PAK

`Create-SIT-PAK` is a _horrifying_ batch file that creates an all-in-one
StayInTarkov folder that contains the source code, modded game, server, and
manager. If you are looking for a way to play the very latest Tarkov Multiplayer
Mod, this is for you. This requires a legal copy of EFT installed from the
Battlestate Games launcher.

**By default, this will install everything to `Desktop\SIT-PAK`. You can change
this by editing the top section of the script.**

This script will:
* Identify where your Live EFT game is installed
* Create the SIT-PAK folder structure
* Clone the [StayInTarkov repositories](https://github.com/stayintarkov/) and
  [SPT-AKI Server](https://dev.sp-tarkov.com/SPT-AKI/Server)
* Copy your Live EFT game to the SIT-PAK
* Install BepInEx modding tools
* Create helper scripts for quality-of-life improvements
* Configure the SIT Manager to work with the SIT-PAK folder structure
* Compile all downloaded repositories:
    * SPT-AKI Server
    * SPT-AKI Modules
    * SIT Aki Server Mod
    * StayInTarkov Client
    * SIT Manager
* Install compiled files

At the end of this process, you will have one `SIT-PAK` folder that you can
play, copy, zip, backup, or whatever with. The SIT-PAK should work on any
computer that has a legal copy of EFT installed.

## Prerequisits

**TODO:** This list is incomplete and should link to download pages.

* git
* git lfs
* visual studio 17
* vscode
* npm
* dotnet?
* winsdk?

## Frequently Asked Questions

### Oh god why is this a batch file?

Because building SIT and making it work is largely _scriptable tasks_. Its a
bunch of "create folder", "run build tool", "copy folder over here"-type stuff.
A heavier "real" scripting language is too much (and I don't want users to
need a specific interpreter just to run some build commands).

### But why is this _batch_ instead of _powershell_?!?

I started writing it in batch and when I realized my mistake, I was halfway
done, so decided to finish the initial project instead of stopping to rewrite
it. Was this a bad idea? Probably? But you have something that works _now_,
even with how ugly the tech behind it is.

If it makes you feel any better, migrating this to powershell is pretty high
on my todo list.

### Why is this installing to my desktop?! Can I change it?!

Yep, edit the line that starts with `set SIT_PAK_INSTALL_LOCATION`.

### Does this have mod support?

Short answer: Maybe.

Long answer: This is a bleeding-edge compile of both SPT-AKI's Server and SIT.
Mods may not be compatible just due to version differences alone. Beyond that,
server mods are more likely to "just work", but most client mods **will not**.
