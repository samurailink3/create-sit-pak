@echo off

REM === Installation Variables ===
set SIT_PAK_INSTALL_LOCATION="%USERPROFILE%\Desktop"
REM === ONLY EDIT THIS PART   ^^^^^^^^^^^^^^^^^^^^^ Make sure to keep the quotes
REM === Example: SIT_PAK_INSTALL_LOCATION="C:\Games\"

REM === Dev Variables ===
set MSBUILD="C:\Program Files\Microsoft Visual Studio\2022\Community\MSBuild\Current\Bin\MSBuild.exe"
set SIT_PAK_LOCATION="%SIT_PAK_INSTALL_LOCATION%\SIT-PAK"
echo ----------------------------------------------------------------------
echo       _/_/_/  _/_/_/  _/_/_/_/_/          _/_/_/      _/_/    _/    _/   
echo    _/          _/        _/              _/    _/  _/    _/  _/  _/      
echo     _/_/      _/        _/  _/_/_/_/_/  _/_/_/    _/_/_/_/  _/_/         
echo        _/    _/        _/              _/        _/    _/  _/  _/        
echo _/_/_/    _/_/_/      _/              _/        _/    _/  _/    _/       
echo ----------------------------------------------------------------------

REM Prompt user and pause
echo This batch file will install a SIT-PAK folder to: %SIT_PAK_INSTALL_LOCATION%
echo To change this location, right-click on this batch file and select 'edit'.
echo You will only need to change one line.
echo If this is the wrong location, please exit this script.
pause

REM Collect system variables
echo =- Collecting system variables
REM ref: https://stackoverflow.com/a/23328830
for /f "tokens=2*" %%a in ('reg query "HKEY_LOCAL_MACHINE\SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Uninstall\EscapeFromTarkov" /v InstallLocation 2^>^&1^|find "REG_"') do @set LIVE_TARKOV_DIRECTORY="%%b"
REM ref: https://stackoverflow.com/a/2544545
if not defined LIVE_TARKOV_DIRECTORY GOTO ERR_TARKOV_NOT_FOUND

REM Create folder structure
echo =- Creating SIT-PAK folder structure
cd %SIT_PAK_INSTALL_LOCATION%
mkdir SIT-PAK\Code
mkdir SIT-PAK\Game\BepInEx\plugins
mkdir SIT-PAK\Manager
mkdir SIT-PAK\Server\user\mods\SITCoop
cd SIT-PAK\

REM Clone repositories
echo =- Cloning repositories
echo =--- SPT-AKI Server
git clone --quiet https://dev.sp-tarkov.com/SPT-AKI/Server Code\\Server
echo =--- SPT-AKI Modules
git clone --quiet https://dev.sp-tarkov.com/SPT-AKI/Modules Code\\Modules
echo =--- SIT.Aki-Server-Mod
git clone --quiet https://github.com/stayintarkov/SIT.Aki-Server-Mod Code\\SIT.Aki-Server-Mod
echo =--- StayInTarkov.Client
git clone --quiet https://github.com/stayintarkov/StayInTarkov.Client Code\\StayInTarkov.Client
echo =--- SIT.Manager
git clone --quiet https://github.com/stayintarkov/SIT.Manager Code\\SIT.Manager

REM Copy Live Tarkov into SIT-PAK
echo =- Copying EFT to SIT-PAK
REM ref: https://stackoverflow.com/a/7487697
robocopy /E %LIVE_TARKOV_DIRECTORY% Game\ /NFL /NDL /NJH /NJS /nc /ns /np
REM Backup original assembly
copy Game\EscapeFromTarkov_Data\Managed\Assembly-CSharp.dll Game\EscapeFromTarkov_Data\Managed\Assembly-CSharp.dll.bak>NUL
echo EscapeFromTarkov.exe: > %SIT_PAK_LOCATION%\VersionTEMP.txt
wmic datafile where Name="%cd:\=\\%\\Game\\EscapeFromTarkov.exe" get Version /format:textvaluelist|more>> %SIT_PAK_LOCATION%\VersionTEMP.txt
findstr "." %SIT_PAK_LOCATION%\VersionTEMP.txt > %SIT_PAK_LOCATION%\Version.txt
del %SIT_PAK_LOCATION%\VersionTEMP.txt

REM Download and install BepInEx
echo =- Downloading and installing BepInEx v5.4.22
powershell -Command "(New-Object Net.WebClient).DownloadFile('https://github.com/BepInEx/BepInEx/releases/download/v5.4.22/BepInEx_x64_5.4.22.0.zip', 'Code\BepInEx_x64_5.4.22.0.zip')"
powershell Expand-Archive Code\BepInEx_x64_5.4.22.0.zip -DestinationPath Game\

REM Install helper batch files
echo =- Creating helper batch files
echo =--- Code\Update-Repositories.bat
echo git -C Server fetch --all>> Code\Update-Repositories.bat
echo git -C Server pull>> Code\Update-Repositories.bat
echo git -C Modules fetch --all>> Code\Update-Repositories.bat
echo git -C Modules pull>> Code\Update-Repositories.bat
echo git -C SIT.Aki-Server-Mod fetch --all>> Code\Update-Repositories.bat
echo git -C SIT.Aki-Server-Mod pull>> Code\Update-Repositories.bat
echo git -C StayInTarkov.Client fetch --all>> Code\Update-Repositories.bat
echo git -C StayInTarkov.Client pull>> Code\Update-Repositories.bat
echo git -C SIT.Manager fetch --all>> Code\Update-Repositories.bat
echo git -C SIT.Manager pull>> Code\Update-Repositories.bat
echo =--- Run-Launcher.bat
echo @echo off>>Run-Launcher.bat
echo echo Launching SIT Manager>>Run-Launcher.bat
echo cd Manager>>Run-Launcher.bat
echo SIT.Manager.exe>>Run-Launcher.bat

REM Create SIT Manager configuration
echo =- Creating SIT Manager configuration file
echo {>>Manager\ManagerConfig.json
echo   "LastServer": "http://127.0.0.1:6969",>>Manager\ManagerConfig.json
echo   "Username": "",>>Manager\ManagerConfig.json
echo   "Password": "",>>Manager\ManagerConfig.json
echo   "InstallPath": "..\\Game",>>Manager\ManagerConfig.json
echo   "AkiServerPath": "..\\Server",>>Manager\ManagerConfig.json
echo   "RememberLogin": false,>>Manager\ManagerConfig.json
echo   "CloseAfterLaunch": false,>>Manager\ManagerConfig.json
echo   "TarkovVersion": null,>>Manager\ManagerConfig.json
echo   "SitVersion": null,>>Manager\ManagerConfig.json
echo   "LookForUpdates": true,>>Manager\ManagerConfig.json
echo   "AcceptedModsDisclaimer": false,>>Manager\ManagerConfig.json
echo   "ModCollectionVersion": null,>>Manager\ManagerConfig.json
echo   "InstalledMods": {},>>Manager\ManagerConfig.json
echo   "ConsoleFontColor": "#FFADD8E6",>>Manager\ManagerConfig.json
echo   "ConsoleFontFamily": "Consolas">>Manager\ManagerConfig.json
echo }>>Manager\ManagerConfig.json

REM Compile Server
echo =- Compiling Server (please wait...)
cd Code\Server\
git checkout --quiet 3.8.0
echo|set /p TEMPVAR=Server: >>%SIT_PAK_LOCATION%\Version.txt
git describe --always>>%SIT_PAK_LOCATION%\Version.txt
cd project
git lfs fetch>NUL
git lfs pull>NUL
REM ref: https://stackoverflow.com/a/42306073
call npm install --silent>NUL
call npm run build:release>NUL
cd %SIT_PAK_LOCATION%
robocopy /E Code\Server\project\build Server\ /NFL /NDL /NJH /NJS /nc /ns /np

REM Compile SPT-AKI Modules
echo =- Compiling SPT-AKI Modules
cd Code\Modules
git checkout --quiet 3.8.0
echo|set /p TEMPVAR=Modules: >>%SIT_PAK_LOCATION%\Version.txt
git describe --always>>%SIT_PAK_LOCATION%\Version.txt
robocopy /E %LIVE_TARKOV_DIRECTORY%\EscapeFromTarkov_Data\Managed project\Shared\Managed /NFL /NDL /NJH /NJS /nc /ns /np
cd project
dotnet build --configuration Release>NUL
cd %SIT_PAK_LOCATION%
copy /Y Code\Modules\project\Build\EscapeFromTarkov_Data\Managed\Aki.Common.dll Game\EscapeFromTarkov_Data\Managed\Aki.Common.dll>NUL
copy /Y Code\Modules\project\Build\EscapeFromTarkov_Data\Managed\Aki.Reflection.dll Game\EscapeFromTarkov_Data\Managed\Aki.Reflection.dll>NUL

REM Compile SIT.Aki-Server-Mod
echo =- Compiling SIT.Aki-Server-Mod (please wait...)
cd Code\SIT.Aki-Server-Mod
echo|set /p TEMPVAR=SIT.Aki-Server-Mod: >>%SIT_PAK_LOCATION%\Version.txt
git describe --always>>%SIT_PAK_LOCATION%\Version.txt
call npm ci --silent
call npm run build>NUL
cd %SIT_PAK_LOCATION%
robocopy /E Code\SIT.Aki-Server-Mod\SITCoop Server\user\mods\SITCoop /NFL /NDL /NJH /NJS /nc /ns /np

REM Compile StayInTarkov.Client
echo =- Compiling StayInTarkov.Client (please wait...)
cd Code\StayInTarkov.Client
echo|set /p TEMPVAR=StayInTarkov.Client: >>%SIT_PAK_LOCATION%\Version.txt
git describe --always>>%SIT_PAK_LOCATION%\Version.txt
%MSBUILD% -t:restore>NUL
%MSBUILD% /nologo /property:Configuration=Release>NUL
cd %SIT_PAK_LOCATION%
copy /Y Code\StayInTarkov.Client\Source\bin\Release\net472\Assembly-CSharp.dll Game\EscapeFromTarkov_Data\Managed\Assembly-CSharp.dll>NUL
copy /Y Code\StayInTarkov.Client\Source\bin\Release\net472\StayInTarkov.dll Game\BepInEx\plugins\StayInTarkov.dll>NUL

REM Compile SIT.Manager
echo =- Compiling SIT.Manager (please wait...)
cd Code\SIT.Manager
%MSBUILD% -t:restore>NUL
%MSBUILD% /nologo /property:Configuration=Release>NUL
cd %SIT_PAK_LOCATION%
robocopy /E Code\SIT.Manager\SIT.Manager.Updater\bin\Release Manager\ /NFL /NDL /NJH /NJS /nc /ns /np
robocopy /E Code\SIT.Manager\SIT.Manager\bin\x64\Release Manager\ /NFL /NDL /NJH /NJS /nc /ns /np

REM Show user completion message and pause
echo === COMPLETE ===
echo Congrats! Your SIT-PAK is installed and ready to configure.
echo Enjoy and good luck!
pause
exit

:ERR_TARKOV_NOT_FOUND
echo Unable to detect your EFT install location.
echo Please make sure you have installed EFT from the Battlestate Games launcher.
echo Without an installed copy of EFT, we cannot continue :(
pause
exit

REM Todos:
REM  - Migrate fully to powershell
REM  - Download and/or Install build tools
REM  - Resumption
REM    - Don't clone if repo folder exists
REM    - Don't copy EFT if EscapeFromTarkov.exe exists
REM  - Ability to use a specific repo location for rapid-testing code changes (dev QoL feature)
REM  - User input / menu system? (could allow people to just update code/rebuild/recopy files)
